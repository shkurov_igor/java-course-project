package Polytech.coursework.articles;

import javax.persistence.*;

@Entity
@Table
public class Articles {
    @Id
    @SequenceGenerator(
            name = "articles_seq",
            allocationSize = 1
    )
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "articles_seq"
    )
    private int id;
    @Column(columnDefinition = "varchar(50)")
    private String name;

    public Articles(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public Articles(String name) {
        this.name = name;
    }

    public Articles() {

    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }


    @Override
    public String toString() {
        return name;
    }
}
