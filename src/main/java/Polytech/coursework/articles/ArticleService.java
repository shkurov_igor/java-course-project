package Polytech.coursework.articles;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class ArticleService {

    private final ArticlesRepository articlesRepository;

    public ArticleService(ArticlesRepository articlesRepository) {
        this.articlesRepository = articlesRepository;
    }

    public List<Articles> getArticles() {
        return articlesRepository.findAll();
    }

    public boolean addNewArticle(String name) {
        Optional<Articles> optional = articlesRepository.findArticlesByName(name);
        if (optional.isPresent()) {
            return false;
        }
        articlesRepository.save(new Articles(name));
        return true;
    }

    public Articles findArticleByName(String name) {
        Optional<Articles> optional = articlesRepository.findArticlesByName(name);
        return optional.orElse(null);
    }
}
