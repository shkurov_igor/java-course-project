package Polytech.coursework.operations;

import Polytech.coursework.articles.Articles;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table
public class Operations {
    @Id
    @SequenceGenerator(
            name = "operations_seq",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "operations_seq"
    )
    private int id;
    @Column(columnDefinition = "numeric(18,2)")
    private float debit;
    @Column(columnDefinition = "numeric(18,2)")
    private float credit;
    private LocalDateTime create_date;
    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    private Articles article;

    public Operations(int id, float debit, float credit, LocalDateTime create_date) {
        this.id = id;
        this.debit = debit;
        this.credit = credit;
        this.create_date = create_date;
    }

    public Operations(float debit, float credit, LocalDateTime create_date) {
        this.debit = debit;
        this.credit = credit;
        this.create_date = create_date;
    }

    public Operations() {

    }

    public int getId() {
        return id;
    }

    public float getDebit() {
        return debit;
    }

    public float getCredit() {
        return credit;
    }

    public LocalDateTime getCreate_date() {
        return create_date;
    }

    public Articles getArticle() {
        return article;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDebit(float debit) {
        this.debit = debit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public void setCreate_date(LocalDateTime create_date) {
        this.create_date = create_date;
    }

    public void setArticle(Articles article) {
        this.article = article;
    }
};
