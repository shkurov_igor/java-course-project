package Polytech.coursework.operations;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class OperationsService {
    private final OperationsRepository operationsRepository;

    public OperationsService(OperationsRepository operationsRepository) {
        this.operationsRepository = operationsRepository;
    }

    public List<Operations> getOperations() {
        return operationsRepository.findAll();
    }

    public void addNewOperation(Operations operation) {
        Optional<Operations> optional = operationsRepository.findOperationsById(operation.getId());
        if (optional.isPresent()) {
            return;
        }
        operationsRepository.save(operation);
    }


    public void deleteOperationById(int id) {
        operationsRepository.deleteOperationsById(id);
    }

    public List<Operations> findOperationsByArticle(String article) {

        return operationsRepository.findAllOperationsByArticleName(article);
    }

    public Operations findOperationById(int id) {
        Optional<Operations> optional = operationsRepository.findOperationsById(id);
        return optional.orElse(null);
    }
}
