package Polytech.coursework.operations;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface OperationsRepository
        extends JpaRepository<Operations, Integer> {
    @Query("select o from Operations o join o.article a where a.name=:name")
    List<Operations> findAllOperationsByArticleName(@Param("name") String name);//!!!

    void deleteOperationsById(int id);

    Optional<Operations> findOperationsById(int id);
}
