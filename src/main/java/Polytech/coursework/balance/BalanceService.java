package Polytech.coursework.balance;

import Polytech.coursework.operations.Operations;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class BalanceService {
    private final BalanceRepository balanceRepository;

    public BalanceService(BalanceRepository balanceRepository) {
        this.balanceRepository = balanceRepository;
    }

    public List<Balance> getBalances() {
        return balanceRepository.findAll();
    }

    public void saveBalance(Balance balance) {
        balanceRepository.save(balance);
    }

    public Balance findBalanceById(int id) {
        Optional<Balance> optional = balanceRepository.findById(id);

        return optional.orElse(null);
    }

    public void updateBalanceAfterAddOperation(Balance balance, Operations operation) { ///!!!
        balance.setAmount(balance.getAmount() - operation.getCredit() + operation.getDebit());
        balance.setCredit(balance.getCredit() + operation.getCredit());
        balance.setDebit(balance.getDebit() + operation.getDebit());
    }

    public void deleteBalanceById(int id) {
        balanceRepository.deleteById(id);
    }

    public void addNewOperationToBalance(Balance balance, Operations operation) {//!!!
        balance.addOperation(operation);
        updateBalanceAfterAddOperation(balance, operation);
    }

    public int countOperationsByBalanceId(int id) {
        return balanceRepository.countOperationsByBalanceId(id);
    }

    public List<Operations> displaySortedOperationsByDateByBalanceId(int id) {
        return balanceRepository.displaySortedOperationsByDateByBalanceId(id);
    }

    public boolean balanceWithHolderExists(String name) {
        return balanceRepository.findBalanceByHolder(name).isPresent();
    }

    public Balance findBalanceByOperation(Operations operation) { //!!!
        Optional<Balance> optional = balanceRepository.findBalanceByOperations(operation);
        return optional.orElse(null);
    }

    public void updateBalanceAfterDeleteOperation(Balance balance, Operations operation) {
        balance.setAmount(balance.getAmount() + operation.getCredit() - operation.getDebit());
        balance.setCredit(balance.getCredit() - operation.getCredit());
        balance.setDebit(balance.getDebit() - operation.getDebit());
    }
}