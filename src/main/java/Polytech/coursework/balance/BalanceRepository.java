package Polytech.coursework.balance;

import Polytech.coursework.operations.Operations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BalanceRepository extends JpaRepository<Balance, Integer> {
    @Query("select size(b.operations) from Balance b where b.id=:id") //!!!
    int countOperationsByBalanceId(@Param("id") int id);

    @Query("select o from Balance b join b.operations o where b.id=:id order by o.create_date desc")//!!!
    List<Operations> displaySortedOperationsByDateByBalanceId(@Param("id") int id);

    Optional<Balance> findBalanceByHolder(String name);

    Optional<Balance> findBalanceByOperations(Operations operation);
}
