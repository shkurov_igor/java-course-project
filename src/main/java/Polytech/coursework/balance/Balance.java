package Polytech.coursework.balance;

import Polytech.coursework.operations.Operations;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Set;

@Entity
@Table
public class Balance {
    @Id
    @SequenceGenerator(
            name = "balance_seq",
            allocationSize = 1)
    @GeneratedValue(
            strategy = GenerationType.SEQUENCE,
            generator = "balance_seq"
    )
    private int id;
    private LocalDateTime create_date;
    @Column(columnDefinition = "numeric(18,2)")
    private float debit;
    @Column(columnDefinition = "numeric(18,2)")
    private float credit;
    @Column(columnDefinition = "numeric(18,2)")
    private float amount;
    @Column(columnDefinition = "varchar(50)")
    private String holder;
    @JoinColumn(name = "balance_id")
    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    private Set<Operations> operations;

    public Balance(int id, LocalDateTime create_date, float debit, float credit, float amount, String holder) {
        this.id = id;
        this.create_date = create_date;
        this.debit = debit;
        this.credit = credit;
        this.amount = amount;
        this.holder = holder;
    }

    public Balance(LocalDateTime create_date, float debit, float credit, float amount, String holder) {
        this.create_date = create_date;
        this.debit = debit;
        this.credit = credit;
        this.amount = amount;
        this.holder = holder;
    }

    public Balance() {}

    public String getHolder() { return holder; }

    public int getId() {
        return id;
    }

    public LocalDateTime getCreate_date() {
        return create_date;
    }

    public float getDebit() {
        return debit;
    }

    public float getCredit() {
        return credit;
    }

    public float getAmount() {
        return amount;
    }

    public Set<Operations> getOperations() {
        return operations;
    }

    public void setHolder(String holder) {
        this.holder = holder;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setCreate_date(LocalDateTime create_date) {
        this.create_date = create_date;
    }

    public void setDebit(float debit) {
        this.debit = debit;
    }

    public void setCredit(float credit) {
        this.credit = credit;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public void addOperation(Operations op) {
        operations.add(op);
    }

    public void setOperations(Set<Operations> operations) {
        this.operations = operations;
    }

    @Override
    public String toString() {
        return "" + amount;
    }
}
