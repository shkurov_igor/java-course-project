package Polytech.coursework.config;

import Polytech.coursework.operations.OperationsRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OperationsConfig {
    @Bean
    CommandLineRunner commandLineRunner(
            OperationsRepository repo) {
        return args -> {

        };
    }
}
