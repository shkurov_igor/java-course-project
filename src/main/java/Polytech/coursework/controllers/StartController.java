package Polytech.coursework.controllers;

import Polytech.coursework.balance.Balance;
import Polytech.coursework.balance.BalanceService;
import Polytech.coursework.user.User;
import Polytech.coursework.user.UserService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Map;

@Controller
@RequestMapping(path = "/")
public class StartController {
    private final BalanceService balanceService;
    private final UserService userService;

    public StartController(BalanceService balanceService, UserService userService) {
        this.balanceService = balanceService;
        this.userService = userService;
    }

    @GetMapping(path = "/login")
    public String loginPage(@RequestParam(required = false) String error,
                            Map<String, Object> model) {
        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        if (error != null) {
            model.put("error_flag", true);
        }

        return "login";
    }

    @GetMapping
    public String getLanding(Map<String, Object> model) {
        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        return "family-budget";
    }

    @GetMapping(path = "/registration")
    public String registerUser(Map<String, Object> model) {
        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        return "registration";
    }

    @PostMapping(path = "/registration")
    public String registerUser(User user,
                               Map<String, Object> model) {
        User dbUser = userService.findUserByUsername(user.getUsername());

        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        if (dbUser != null) {
            model.put("user_exists", true);
            return "/registration";
        }

        userService.addNewUser(user);

        return "redirect:/login";
    }
}
