package Polytech.coursework.controllers;

import Polytech.coursework.articles.ArticleService;
import Polytech.coursework.articles.Articles;
import Polytech.coursework.balance.Balance;
import Polytech.coursework.balance.BalanceService;
import Polytech.coursework.operations.Operations;
import Polytech.coursework.operations.OperationsService;
import Polytech.coursework.user.UserService;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.transaction.Transactional;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

@Controller
@RequestMapping(path = "/family-budget")
public class MainController {

    private final OperationsService operationsService;
    private final BalanceService balanceService;
    private final UserService userService;

    public MainController(OperationsService operationsService, BalanceService balanceService, ArticleService articleService, UserService userService) {
        this.operationsService = operationsService;
        this.balanceService = balanceService;
        this.userService = userService;
    }

    @GetMapping
    public String getLanding(Map<String, Object> model) {
        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        return "family-budget";
    }

    @GetMapping(path = "/operations")
    public String operationsPage(Map<String, Object> model) {
        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        return "operations";
    }

    @GetMapping(path = "/lk")
    public String lkPage(@RequestParam(value = "id", required = false) String id,
                         Map<String, Object> model) {
        Iterable<Balance> balances = balanceService.getBalances();

        model.put("balances", balances);

        boolean flag = false;

        if (id != null) {
            int index = Integer.parseInt(id);
            List<Operations> operations_sorted = balanceService.displaySortedOperationsByDateByBalanceId(index);
            operations_sorted = operations_sorted.stream().limit(3).collect(Collectors.toList());

            model.put("operations_sorted", operations_sorted);
            model.put("count", balanceService.countOperationsByBalanceId(index));
            model.put("selected", index);

            flag = true;
        }
        model.put("post_flag", flag);

        return "lk";
    }

    @PostMapping(path = "/lk")
    public String searchLk(@RequestParam String name,
                           @RequestParam int id,
                           Map<String, Object> model) {
        Iterable<Balance> balances = balanceService.getBalances();
        List<Operations> operations_sorted = balanceService.displaySortedOperationsByDateByBalanceId(id);
        operations_sorted = operations_sorted.stream().limit(3).collect(Collectors.toList());

        model.put("balances", balances);
        model.put("operations_sorted", operations_sorted);

        boolean operations_flag = true;

        model.put("count", balanceService.countOperationsByBalanceId(id));
        model.put("selected", id);

        Iterable<Operations> operations = operationsService.findOperationsByArticle(name);
        if (operations != null) {
            model.put("operations", operations);
        }
        else {
            operations_flag = false;
        }

        model.put("operations_flag", operations_flag);
        model.put("post_flag", true);

        return "lk";
    }

    @Transactional
    @PostMapping(path = "/lk", params = "delete_account")
    public String deleteAccount() {
        Authentication loggedInUser = SecurityContextHolder.getContext().getAuthentication();
        String username = loggedInUser.getName();

        userService.deleteUserByUsername(username);

        return "redirect:/login";
    }

    @GetMapping(path = "/balance")
    public String balancePage(Map<String, Object> model) {
        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        return "balance";
    }

    @PostMapping(path = "/balance", params = "add")
    public String addBalance(@RequestParam String name,
                             Map<String, Object> model) {
        Balance balance = new Balance(LocalDateTime.now(), 0, 0, 0, name);

        if (!balanceService.balanceWithHolderExists(name)) {
            balanceService.saveBalance(balance);
        }
        else {
            model.put("user_exists", true);
        }

        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        return "balance";
    }

    @Transactional
    @PostMapping(path = "/balance", params = "delete")
    public String deleteBalance(@RequestParam int balance_id,
                                Map<String, Object> model) {
        balanceService.deleteBalanceById(balance_id);

        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        return "balance";
    }

    @PostMapping(path = "/operations", params = "add")
    public String addOperation(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime create_date,
                               @RequestParam(required = false) float debit,
                               @RequestParam(required = false) float credit,
                               @RequestParam String name,
                               @RequestParam int balance_id,
                               Map<String, Object> model) {
        Articles article = new Articles(name);
        Balance balance = balanceService.findBalanceById(balance_id);

        Operations operation = new Operations(debit, credit, create_date);
        operation.setArticle(article);

        balanceService.addNewOperationToBalance(balance, operation);
        operationsService.addNewOperation(operation);

        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        return "operations";
    }

    @Transactional
    @PostMapping(path = "/operations", params = "delete")
    public String deleteOperation(@RequestParam int operation_id,
                                  Map<String, Object> model) {
        Operations operation = operationsService.findOperationById(operation_id);
        Balance balance = balanceService.findBalanceByOperation(operation);

        Set<Operations> set = balance.getOperations();
        set.remove(operation);
        balance.setOperations(set);

        balanceService.updateBalanceAfterDeleteOperation(balance, operation);
        operationsService.deleteOperationById(operation_id);

        Iterable<Balance> balances = balanceService.getBalances();
        model.put("balances", balances);

        return "operations";
    }
}
